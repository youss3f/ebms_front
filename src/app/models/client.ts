import {Bank} from "./bank";

export class Client {
  id: string = "";
  firstName: string = "";
  lastName: string = "";
  phoneNumber: string = "";
  nationality: string = "";
  residenceAddress: string = "";
  birthDate: Date = new Date();
  email: string = "";
  password: string = "";
  gender: string = "";
  bank: Bank = new Bank();
}
