import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Bank} from "../models/bank";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BankService {
  BASE_URL = "http://localhost:8081/api/v1/banks";

  constructor(private http: HttpClient) { }

  public getBankById(bankId: string): Observable<any> {
    return this.http.get(this.BASE_URL + "/" + bankId);
  }

  public getBanks(): Observable<any> {
    return this.http.get(this.BASE_URL);
  }

  public addBank(bank: Bank): Observable<any> {
    return this.http.post(this.BASE_URL, bank);
  }

  public deleteBank(bankId: string) {
    return this.http.delete(this.BASE_URL + "/" + bankId);
  }
}
